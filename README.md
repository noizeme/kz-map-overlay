# kz-map-overlay

Uses [GSISocket](https://bitbucket.org/Sikarii/gsisocket/) and displays mapname, wrs... as html

### Requirements
- [GSISocket](https://bitbucket.org/Sikarii/gsisocket/)

### Features
- Mappings for values and their CSS (See [config](https://bitbucket.org/Sikarii/kz-map-overlay/src/master/js/config.js))

